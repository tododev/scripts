#!/bin/bash
NEW_DB_USER="user"
NEW_DB_USER_PASSWORD="user_password"
POSTGRES_USER_PASSWORD="postgres"
# Update package list and install prerequisites
echo "Updating system and installing prerequisites..."
sudo apt update && sudo apt upgrade -y
sudo apt install -y wget gnupg2

# Add PostgreSQL APT repository
echo "Adding PostgreSQL repository..."
wget -qO - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

# Update package list again to include new PostgreSQL packages
echo "Updating package list for PostgreSQL 17..."
sudo apt update

# Install PostgreSQL 17
echo "Installing PostgreSQL 17..."
sudo apt install -y postgresql-17

# Enable and start PostgreSQL service
echo "Enabling and starting PostgreSQL service..."
sudo systemctl enable postgresql
sudo systemctl start postgresql

# Set password for default PostgreSQL user
echo "Setting password for default PostgreSQL user..."
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD '$POSTGRES_USER_PASSWORD';"

# Create a new PostgreSQL user
echo "Creating a new PostgreSQL user..."
sudo -u postgres psql -c "CREATE USER $NEW_DB_USER WITH PASSWORD '$NEW_DB_USER_PASSWORD';"


# Confirm installation
echo "PostgreSQL 17 installation complete."
echo "PostgreSQL version:"
psql --version

# Configuration pour permettre les connexions distantes
echo "Configuration des accès distants dans postgresql.conf..."
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/17/main/postgresql.conf

echo "Configuration des accès distants dans pg_hba.conf..."
echo "host    all             all             0.0.0.0/0               md5" | sudo tee -a /etc/postgresql/17/main/pg_hba.conf

# Optimisation des paramètres PostgreSQL en évitant les doublons
echo "Optimisation des paramètres dans postgresql.conf..."

# config 8Go RAM, 12 coeurs
sudo sed -i -E "
s/#?max_connections = .*/max_connections = 100/;
s/#?shared_buffers = .*/shared_buffers = 2GB/;
s/#?effective_cache_size = .*/effective_cache_size = 6GB/;
s/#?maintenance_work_mem = .*/maintenance_work_mem = 512MB/;
s/#?checkpoint_completion_target = .*/checkpoint_completion_target = 0.9/;
s/#?wal_buffers = .*/wal_buffers = 16MB/;
s/#?default_statistics_target = .*/default_statistics_target = 100/;
s/#?random_page_cost = .*/random_page_cost = 1.1/;
s/#?effective_io_concurrency = .*/effective_io_concurrency = 200/;
s/#?work_mem = .*/work_mem = 32MB/;
s/#?huge_pages = .*/huge_pages = try/;
s/#?min_wal_size = .*/min_wal_size = 1GB/;
s/#?max_wal_size = .*/max_wal_size = 4GB/;
s/#?max_worker_processes = .*/max_worker_processes = 12/;
s/#?max_parallel_workers_per_gather = .*/max_parallel_workers_per_gather = 4/;
s/#?max_parallel_workers = .*/max_parallel_workers = 12/;
s/#?max_parallel_maintenance_workers = .*/max_parallel_maintenance_workers = 4/;
" /etc/postgresql/17/main/postgresql.conf

# Redémarrage du service PostgreSQL pour appliquer les modifications
echo "Redémarrage du service PostgreSQL..."
sudo systemctl restart postgresql

# Configuration du pare-feu pour autoriser le port 5432
echo "Ouverture du port 5432 dans le pare-feu..."
sudo ufw allow 5432/tcp

# Récupérer l'adresse IP de la VM
SERVER_IP=$(hostname -I | awk '{print $1}')

# Confirmation d'installation
echo "Installation et configuration de PostgreSQL terminées."
echo "Version de PostgreSQL installée :"
psql --version

echo "Vous pouvez maintenant vous connecter à PostgreSQL à distance avec la commande suivante :"
echo "psql -h $SERVER_IP -U $NEW_DB_USER -d postgres"
#!/bin/bash

# Installer Elasticsearch
echo "Installation d'Elasticsearch..."
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get update && sudo apt-get install elasticsearch

# Démarrer et activer Elasticsearch au démarrage
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service

# Installer Kibana
echo "Installation de Kibana..."
sudo apt-get install kibana

# Démarrer et activer Kibana au démarrage
sudo systemctl enable kibana.service
sudo systemctl start kibana.service

# Installer Logstash
echo "Installation de Logstash..."
sudo apt-get install logstash

# Configurer Logstash (exemple basique)
echo "input {
  beats {
    port => 5044
  }
}
output {
  elasticsearch {
    hosts => ["localhost:9200"]
    manage_template => false
    index => "%{[@metadata][beat]}-%{+YYYY.MM.dd}"
    document_type => "%{[@metadata][type]}"
  }
}" | sudo tee /etc/logstash/conf.d/logstash.conf

# Démarrer et activer Logstash au démarrage
sudo systemctl enable logstash.service
sudo systemctl start logstash.service

echo "Installation d'Elastic Stack terminée."

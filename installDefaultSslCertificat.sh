#!/bin/bash

# Vérifie si le nombre de paramètres attendus est correct
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 domain_name email_address"
    exit 1
fi

# Récupère le nom de domaine et l'adresse e-mail à partir des paramètres
DOMAIN=$1
EMAIL=$2

# Met à jour les paquets et installe Certbot
echo "Mise à jour des paquets et installation de Certbot..."
sudo apt-get update
sudo apt-get install -y certbot python3-certbot-nginx

# Obtient un certificat SSL pour le domaine et configure NGINX automatiquement
echo "Obtention d'un certificat SSL pour $DOMAIN..."
sudo certbot --nginx -m $EMAIL --agree-tos --no-eff-email -d $DOMAIN -d www.$DOMAIN

# Vérification de la réussite de l'installation du certificat
if [ $? -ne 0 ]; then
    echo "La tentative d'obtention d'un certificat SSL pour $DOMAIN a échoué."
    exit 1
else
    echo "Certificat SSL obtenu et configuré avec succès pour $DOMAIN."
fi

# Planification du renouvellement automatique du certificat
echo "Planification du renouvellement automatique du certificat..."
(crontab -l 2>/dev/null; echo "0 12 * * * certbot renew --quiet") | crontab -

echo "Tout est configuré ! Votre site https://$DOMAIN est maintenant sécurisé avec SSL."

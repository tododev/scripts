#!/bin/bash

# Chemin vers le fichier de configuration SSH
SSH_CONFIG_FILE="/etc/ssh/sshd_config"

# Faire une copie de sauvegarde du fichier de configuration SSH
sudo cp $SSH_CONFIG_FILE ${SSH_CONFIG_FILE}.bak

# Désactiver la connexion SSH pour l'utilisateur root via mot de passe
sudo sed -i 's/#PermitRootLogin yes/PermitRootLogin prohibit-password/g' $SSH_CONFIG_FILE
sudo sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/g' $SSH_CONFIG_FILE

# Redémarrer le service SSH pour appliquer les changements
sudo systemctl restart sshd

echo "La connexion SSH pour l'utilisateur root via mot de passe a été désactivée."

#!/bin/bash

# Variables de configuration
DB_USER="tododata"
DB_NAME="historique_cadastre"
POSTGRES_USER_PASSWORD="postgres"

# Mettre à jour le système
echo "Mise à jour du système..."
sudo apt update && sudo apt upgrade -y

# Installer les dépendances nécessaires pour PostgreSQL, PostGIS, GDAL et Python
echo "Installation des dépendances..."
sudo apt install -y wget gnupg2 build-essential libxml2-dev libproj-dev libgdal-dev libjson-c-dev \
libgeos-dev libprotobuf-c-dev postgresql-server-dev-17 protobuf-c-compiler python3-dev python3-pip python3-venv \
gdal-bin proj-bin unzip

# Télécharger et installer PostgreSQL 17
echo "Installation de PostgreSQL 17..."
sudo apt install -y postgresql-17 postgresql-client-17

# Télécharger et compiler PostGIS 3.5.0
echo "Téléchargement des sources de PostGIS 3.5.0..."
wget https://download.osgeo.org/postgis/source/postgis-3.5.0.tar.gz
tar -xvf postgis-3.5.0.tar.gz
cd postgis-3.5.0

echo "Compilation et installation de PostGIS 3.5.0..."
./configure --with-pgconfig=/usr/lib/postgresql/17/bin/pg_config --without-protobuf
make -j$(nproc)
sudo make install

# Nettoyer les fichiers téléchargés
cd ..
rm -rf postgis-3.5.0 postgis-3.5.0.tar.gz

# Redémarrer PostgreSQL
echo "Redémarrage du service PostgreSQL..."
sudo systemctl restart postgresql

# Création d'un utilisateur et d'une base de données
echo "Création de l'utilisateur et de la base de données..."
sudo -u postgres psql -c "CREATE USER $DB_USER WITH PASSWORD '$POSTGRES_USER_PASSWORD';"
sudo -u postgres psql -c "CREATE DATABASE $DB_NAME OWNER $DB_USER;"

# Activer les extensions dans la base de données
echo "Activation des extensions PostGIS..."
sudo -u postgres psql -d $DB_NAME -c "CREATE EXTENSION IF NOT EXISTS postgis;"
sudo -u postgres psql -d $DB_NAME -c "CREATE EXTENSION IF NOT EXISTS postgis_topology;"
sudo -u postgres psql -d $DB_NAME -c "CREATE EXTENSION IF NOT EXISTS pgrouting;"

# Vérification des extensions installées
echo "Vérification des extensions installées..."
sudo -u postgres psql -d $DB_NAME -c "\dx"

# Afficher l'adresse IP pour la connexion PostgreSQL
SERVER_IP=$(hostname -I | awk '{print $1}')
echo "Installation terminée."
echo "Pour vous connecter à la base de données, utilisez :"
echo "psql -h $SERVER_IP -U $DB_USER -d $DB_NAME"

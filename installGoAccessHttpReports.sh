#!/bin/bash

# Vérifier si un argument a été fourni
if [ $# -eq 0 ]; then
    echo "Usage: $0 {nginx|apache}"
    exit 1
fi

# Installer GoAccess
echo "Installation de GoAccess..."
sudo apt-get update
sudo apt-get install -y goaccess

# Configurer GoAccess en fonction du serveur web spécifié
case $1 in
    nginx)
        LOG_PATH="/var/log/nginx/access.log"
        ;;
    apache)
        LOG_PATH="/var/log/apache2/access.log"
        ;;
    *)
        echo "Serveur web non reconnu. Utilisez 'nginx' ou 'apache'."
        exit 1
        ;;
esac

# Créer un rapport HTML en temps réel
REPORT_PATH="/var/www/html/report.html"

echo "Configuration de GoAccess pour utiliser les logs $1..."
sudo goaccess $LOG_PATH -o $REPORT_PATH --log-format=COMBINED --real-time-html &

echo "GoAccess a été configuré pour surveiller les logs de $1. Rapport disponible à $REPORT_PATH"

# Le but est d'utiliser WSL pour installer Debian et gérer plusieurs projets avec plusieurs repots en Docker context.


# 1. Installer WSL
# Télécharger WSL2 / activer sous Windows 11

# 2. Installer Debian
# wsl --install -d Debian
# Vérifier que Debian est installé en WSL2
# wsl --list --verbose
# Si version 1, passer en version 2 : wsl --set-version Debian 2
# Définir Debian comme version par défaut : wsl --set-default Debian

# 3. Installer Docker Desktop
# Télécharger Docker Desktop et modifier les paramètres pour utiliser WSL2
# Settings > General → Activer "Use the WSL2 based engine"
# Settings > Resources > WSL Integration → Activer Debian.
# Appliquer et redémarrer Docker Desktop.

# 4. Optimisation de l'utilisation de WSL2
# Créer ou éditer C:\Users\username\.wslconfig avec le contenu suivant :
# [wsl2]
#memory=16GB   # Limite l'utilisation de la RAM (ajuster selon besoin)
#processors=8  # Nombre de cœurs CPU
#swap=0        # Désactiver le swap pour de meilleures performances

# puis redémarrer WSL2 : wsl --shutdown

# 5. Installer Docker dans Debian
sudo apt update && sudo apt install -y docker.io
sudo usermod -aG docker $USER
newgrp docker
docker --version
# Vérifier que Docker fonctionne
docker run hello-world

# 6. Clone les projets
# git clone (url du projet)

# 7. Créer des context par projet
# docker context create cvc
# docker context create freelance

# 8. Utiliser / basculer entre les contextes
# docker context use cvc

# 9. Lancer les services
# cd cvc
# docker compose up -d

# 10. Utils
# docker context ls # Liste les contextes et indique celui qui est actif avec un *
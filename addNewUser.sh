#!/bin/bash

# Vérifie si le script est exécuté en tant que root
if [[ $(id -u) -ne 0 ]]; then
    echo "Ce script doit être exécuté avec des privilèges root."
    exit 1
fi

# Vérifie le nombre de paramètres
if [ "$#" -lt 2 ]; then
    echo "Usage: $0 username password [sudo]"
    exit 1
fi

# Lit les paramètres
USERNAME=$1
PASSWORD=$2
SUDO_OPTION=$3

# Crée l'utilisateur
useradd -m -s /bin/bash "$USERNAME"

# Définit le mot de passe pour l'utilisateur
echo "$USERNAME:$PASSWORD" | chpasswd

# Ajoute l'utilisateur au groupe sudo, si demandé
if [ "$SUDO_OPTION" = "sudo" ]; then
    usermod -aG sudo "$USERNAME"
    echo "$USERNAME ajouté au groupe sudo."
else
    echo "$USERNAME créé sans privilèges sudo."
fi

echo "Utilisateur $USERNAME créé avec succès."

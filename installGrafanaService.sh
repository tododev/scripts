#!/bin/bash

# Ajouter le dépôt Grafana
echo "Installation de Grafana..."
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee /etc/apt/sources.list.d/grafana.list

# Mettre à jour les paquets et installer Grafana
sudo apt-get update
sudo apt-get install -y grafana

# Démarrer et activer Grafana au démarrage
sudo systemctl enable grafana-server.service
sudo systemctl start grafana-server.service

echo "Grafana installé et en cours d'exécution."

#!/bin/bash

# Vérifie si le nombre de paramètres attendus est correct
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 domain_name internal_redirect_address"
    exit 1
fi

# Récupère le nom de domaine et l'adresse de redirection à partir des paramètres
DOMAIN_NAME=$1
INTERNAL_REDIRECT=$2

# Chemin du fichier de configuration NGINX pour le domaine
NGINX_CONFIG_FILE="/etc/nginx/sites-available/$DOMAIN_NAME.conf"

# Crée la configuration NGINX
cat > $NGINX_CONFIG_FILE <<EOF
server {
    listen 80;
    listen [::]:80;
    server_name $DOMAIN_NAME www.$DOMAIN_NAME;

    # Redirection de HTTP à HTTPS
    return 301 https://\$server_name\$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name $DOMAIN_NAME www.$DOMAIN_NAME;

    # Chemins pour les certificats SSL, à remplacer par vos propres chemins de fichier
    ssl_certificate /chemin/vers/votre/certificat/fullchain.pem;
    ssl_certificate_key /chemin/vers/votre/certificat/privkey.pem;

    # Configuration SSL forte
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers 'ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384';
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;

    # Paramètres de performance et de sécurité
    add_header Strict-Transport-Security "max-age=31536000" always;
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";

    # Compression Gzip
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    # Compression Brotli si votre NGINX a été compilé avec le support de Brotli
    brotli on;
    brotli_comp_level 6;
    brotli_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    location / {
        proxy_pass http://$INTERNAL_REDIRECT;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

# Vérifie la syntaxe de la configuration NGINX
nginx -t

# Affiche un message de succès
echo "La configuration NGINX pour $DOMAIN_NAME a été créée avec succès."

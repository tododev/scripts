#!/bin/bash

# Vérifie si Certbot est installé et installe-le si nécessaire
if ! command -v certbot &> /dev/null
then
    echo "Certbot n'est pas installé. Installation de Certbot..."
    sudo apt-get update
    sudo apt-get install -y certbot
fi

# Ajoute une tâche cron pour le renouvellement automatique des certificats
# Certbot recommande de lancer deux fois par jour le processus de renouvellement
(crontab -l 2>/dev/null; echo "0 0,12 * * * certbot renew --quiet") | crontab -

echo "Tâche cron configurée pour le renouvellement automatique des certificats SSL avec Certbot."

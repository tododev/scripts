#!/bin/bash

# Le nom du fichier sera le nom de la commande donc renommer le fichier en switch-project.sh


# Liste des projets Docker
PROJECTS=("cvc" "freelance" "tododata" "template")

# Vérifier si un projet a été fourni
if [ -z "$1" ]; then
  echo "Usage: switch-project <projet>"
  echo "Projets disponibles: ${PROJECTS[*]}"
  exit 1
fi

# Vérifier si le projet existe
if [[ ! " ${PROJECTS[*]} " =~ " $1 " ]]; then
  echo "Erreur: Projet '$1' non trouvé. Projets disponibles: ${PROJECTS[*]}"
  exit 1
fi

# Switcher vers le bon contexte Docker
echo "Switch vers le projet '$1'..."
docker context use "$1"

# Aller dans le dossier du projet
cd ~/"$1" || exit
echo "Positionné dans ~/$1"

# Afficher le contexte actif
docker context ls | grep '*'

# Vérifier qu'il y a un fichier docker-compose.yml
if [ ! -f "docker-compose.yml" ]; then
  echo "Attention: Aucun fichier docker-compose.yml trouvé."
else
  echo "Fichier docker-compose.yml trouvé."
  # Vérifier si le conteneur est en cours d'exécution
  if docker-compose ps | grep "Up" >/dev/null; then
    echo "Conteneur déjà en cours d'éxécution."
  else
    echo "Le conteneur n'est pas en cours d'exécution. Démarrage..."
    docker-compose up -d
  fi
fi

# Rentre le script exécutable
# chmod +x ~/bin/switch-project.sh
# Ajoute le dossier ~/bin/ dans le PATH
# export PATH="$HOME/bin:$PATH"
# Recharge le fichier .bashrc
# source ~/.bashrc

# Appeler le script depuis powershell avec la commande switch-project.sh <projet>
# Soit : wsl ~/bin/switch-project.sh cvc
# Créer une fonction PowerShell pour appelé le script
# function Switch-WSLProject {
#     param (
#         [string]$Project
#     )

#     if (-not $Project) {
#         Write-Host "Usage: switch-project <project-name>"
#         return
#     }

#     Write-Host "🔄 Switching to project '$Project' in WSL..."
#     wsl ~/bin/switch-project.sh $Project
# }

# Set-Alias switch-project Switch-WSLProject

# Vérifier qu'on peut appeler la commande switch-project depuis PowerShell
# Get-Command switch-project

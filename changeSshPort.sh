#!/bin/bash

# Vérifie si le nombre de paramètres est correct
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 new_ssh_port"
    exit 1
fi

NEW_SSH_PORT=$1
SSH_CONFIG_FILE="/etc/ssh/sshd_config"

# Vérification de la présence de UFW et si il est actif
UFW_ACTIVE=$(sudo ufw status | grep -o "active")

# Faire une copie de sauvegarde du fichier de configuration SSH
sudo cp $SSH_CONFIG_FILE ${SSH_CONFIG_FILE}.bak

# Modifier le port SSH dans le fichier de configuration
sudo sed -i "/^#Port 22/c\Port $NEW_SSH_PORT" $SSH_CONFIG_FILE

# Redémarrer le service SSH pour appliquer les changements
sudo systemctl restart sshd
echo "Le port SSH a été changé en $NEW_SSH_PORT."

# Mettre à jour les règles UFW si UFW est actif
if [ "$UFW_ACTIVE" = "active" ]; then
    echo "UFW détecté comme actif. Mise à jour des règles UFW..."
    # Supprimer l'ancienne règle pour le port 22
    sudo ufw delete allow 22
    # Ajouter une nouvelle règle pour le nouveau port SSH
    sudo ufw allow $NEW_SSH_PORT
    echo "Règles UFW mises à jour pour permettre le trafic sur le port $NEW_SSH_PORT."
else
    echo "UFW n'est pas actif ou pas installé. Aucune action n'a été prise sur UFW."
fi
